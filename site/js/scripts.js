$(function () {

    // аккордеон

    $(".title-item-tips-bl-js").on('click', function(e) {
        e.preventDefault();
        if($(this).next("div").is(":visible")){
            $(this).next("div").slideUp(200);
            $(this).removeClass("active");

        } else {
            $(".content-item-tips-bl-js").slideUp(200);
            $(this).next("div").slideDown(200);
            $(this).parents().siblings().children(".title-item-tips-bl-js").removeClass("active");
            $(this).addClass("active");


        }
    });

    $('.form-check-inp').on('change', function(event) {
       var num = $('.form-check-inp:checked').length;

       if (num > 0) {
        $('.form-section-content-cont-center').slideDown(400);
        $(this).parents('form').find('.button-style').prop('disabled', false);
        $(this).parents('form').find('.button-style').attr('disabled', false);
        
        
       }
       else{
        $('.form-section-content-cont-center').slideUp(400);
        $(this).parents('form').find('.button-style').prop('disabled', true);
        $(this).parents('form').find('.button-style').attr('disabled', true);
       }
    });


    if ($("#zdrop_file").length > 0) {
        var zdrop_file;
        initFileUploader("#zdrop_file");
        function initFileUploader(target) {
            var previewNode = document.querySelector(".zdrop-template-file");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            zdrop_file = new Dropzone(target, {
                url: 'upload.php',
                maxFilesize:10,
                paramName: 'file',
                maxFilesize:25,
                acceptedFiles: "image/jpeg,image/png,image/gif",
                previewTemplate: previewTemplate,
                previewsContainer: ".previews-file-file",
                hiddenInputContainer:'.file-page-file',
                clickable: "#upload-label-file",
                chunksUploaded: function(file, done) {
                     
                }
            });

            zdrop_file.on("addedfile", function(file) {
                var bl = $('.zdrop-template-file').length;
                if (bl > 0) {
                    $('.title-form-section-content-cont-js').addClass('active');
                }
                else{
                    $('.title-form-section-content-cont-js').removeClass('active');
                }
            });

            zdrop_file.on("removedfile", function(file) {
                var bl = $('.zdrop-template-file').length;
                if (bl == 0) {
                    $('.title-form-section-content-cont-js').removeClass('active');
                }
            });

            
    
          }
    }

    

    if ($(".slider-reviews").length > 0) {
        $('.slider-reviews').each(function(index, value) {
            var swiper = new Swiper(value, {
                slidesPerView: 1,
                spaceBetween: 20,
                loop: true,
                slidesPerGroup: 1,
                observer: true, 
                observeParents: true,
                navigation: {
                    nextEl: value.nextElementSibling.nextElementSibling,
                    prevEl: value.nextElementSibling,
                },
            });
         
      });

    }

    if ($(".slider-bookkeeping").length > 0) {
        $('.slider-bookkeeping').each(function(index, value) {
            var swiper2 = new Swiper(value, {
                slidesPerView: 1,
                spaceBetween: 20,
                loop: true,
                lazy: true,
                slidesPerGroup: 1,
                observer: true, 
                observeParents: true,
                navigation: {
                    nextEl: value.nextElementSibling.nextElementSibling,
                    prevEl: value.nextElementSibling,
                },
            });
         
        });

    }

    if ($(".slider-year-top").length > 0) {
        var galleryThumbs;
        var galleryTop;
        $('.slider-year-top').each(function(index, value) {
            var thumbs = $(value).parents('.slider-year-container').find('.slider-year-thumbs');
            galleryThumbs = new Swiper(thumbs, {
                spaceBetween: 0,
                slidesPerView: 5,
                loop: false,
                initialSlide: 4,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
                breakpoints: {
                    768: {
                        slidesPerView: 4,
                    },
                    550: {
                        slidesPerView: 3,
                    },
                    400: {
                        slidesPerView: 2,
                    },
                }
                  
            });

            galleryTop = new Swiper(value, {
                spaceBetween: 10,
                loop:true,
                loopedSlides: 5,
                initialSlide: 4,
                navigation: {
                    nextEl: value.nextElementSibling.nextElementSibling,
                    prevEl: value.nextElementSibling,
                },
                thumbs: {
                    swiper: galleryThumbs,
                },
                breakpoints: {
                    768: {
                        autoHeight: true,
                    },
                }
            });
         
        });
 
    }


    $( ".selectmenu" ).selectmenu();


    if ($(".slider-ui-js").length > 0){
        var slider = $('.slider-ui-js');
        slider.each(function(index, el) {
            $(el).parents('.top-slider-calc').find('.val-slider-js').text(parseInt($(el).attr('data-value')));
            $(el).parents('.top-slider-calc').find('.inp_slider_js').val(parseInt($(el).attr('data-value')));
            $(el).slider({
                value: parseInt($(el).attr('data-value')),
                range: "min",
                min: parseInt($(el).attr('data-min')),
                max: parseInt($(el).attr('data-max')),
                slide: function(event, ui) {
                    $(this).parents('.top-slider-calc').find('.val-slider-js').text(ui.value);
                    $(this).parents('.top-slider-calc').find('.inp_slider_js').val(ui.value);
                }
            });


            $(el).parents('.slider-block-wrap').find('.btn-slider-minus').on('click', function(event) {
                $(el).slider( "value", $(el).slider( "value" ) - 5 );
                $(this).parents('.top-slider-calc').find('.val-slider-js').text(parseInt($(el).slider( "value")));
                $(this).parents('.top-slider-calc').find('.inp_slider_js').val(parseInt($(el).slider( "value")));
            });
            $(el).parents('.slider-block-wrap').find('.btn-slider-plus').on('click', function(event) {
                $(el).slider( "value", $(el).slider( "value" ) + 5 );
                $(this).parents('.top-slider-calc').find('.val-slider-js').text(parseInt($(el).slider( "value")));
                $(this).parents('.top-slider-calc').find('.inp_slider_js').val(parseInt($(el).slider( "value")));
            });


            function textSlider(){
                var valSlider = $(el).slider("value").toFixed();

                if (valSlider != 2 && valSlider != 3 && valSlider != 4) {
                    $('.text-slider-js').text('человек');
                }
                else{
                    $('.text-slider-js').text('человека');
                }
            }
            textSlider();
            $(el).on( "slidechange", textSlider);




        });
  
    }


    $('ul.list-tab-reviews a').click(function(e) {
        e.preventDefault();
        $('ul.list-tab-reviews .active').removeClass('active');
        $(this).addClass('active');
        var tab = $(this).attr('href');
        $('.item-content-review').not(tab).css({'display':'none'});
        $(tab).fadeIn(400);
    });


    // Боковое меню

    $('.menuToggle').on('click', function() {
        $('body').toggleClass('none-scroll');
        $('.main-nav').toggleClass('active');
        $('.overlay-page').toggleClass('active');
        $('.menuToggle').toggleClass('active');
    });


    $('.overlay-page').on('click', function() {
        $('body').removeClass('none-scroll');
        $('.main-nav').removeClass('active');
        $('.menuToggle').removeClass('active');
        $(this).removeClass('active');

        
    });

    $('.btn-info-calc-block-js').on('click', function(event) {
        $(this).next().slideToggle(50);
    });


    function scrollPrice(){
        var block = $('.price-main-block-calc-js');
        var blockH = $('.price-main-block-calc-js').innerHeight();

        var the_top = $(document).scrollTop();
        var top = $('.price-main-block-calc-wrap').offset().top;
        var blockW = $('.price-main-block-calc-wrap').innerWidth();
        var headerH = $('.header-page').innerHeight();

        var heightParent = $('.right-form-calc').innerHeight();
        var topParent = $('.right-form-calc').offset().top;

        var arentBlock = topParent + heightParent;
        var activeBlock = the_top + headerH;

        var blockTop = the_top + headerH + blockH;


        var topMobile = $('.block-price-calc-mobile').offset().top;


        if (window.matchMedia('(min-width: 851px)').matches){
        	if (top < activeBlock) {
	            if(arentBlock <  blockTop){
	                block.addClass('absol');
	                block.removeClass('fixed');
	                block.css('top', 'auto');
	                block.css('width', '100%');
	            }
	            else{
	                block.addClass('fixed');
	                block.removeClass('absol');
	                block.css('top', headerH + 'px');
	                block.css('width', blockW + 'px');
	            }
	            
	        }
	        
	        else{
	            block.removeClass('fixed');
	            block.removeClass('absol');
	            block.css('top', 'auto');
	            block.css('width', 'auto');
	        }
        }
        else{
            console.log(topParent);
            console.log(topMobile);

            if (topMobile >= topParent) {
                $('.block-price-calc-mobile').addClass('none_active');
            }
            else{
                $('.block-price-calc-mobile').removeClass('none_active');
            }
           
        }
    }

    if ($(".price-main-block-calc-js").length > 0){
        scrollPrice();

        $(document).on('scroll', scrollPrice);
        $(window).on('resize', scrollPrice);
    }



    $(".form-modal").each(function(index, el) {
        $(el).validate({
            rules: {
                "name_user": {
                    required: true,
                    minlength: 1,
                },
                "tel_user": {
                    required: true,
                    minlength: 10,
                },
                "name_company": {
                    required: true,
                    minlength: 1,
                },
                "email_user": {
                    email: true,
                    required: true,
                }
            },
            ignore: ':hidden:not(:checkbox)',
            messages: {
                "name_user": {
                    required: "Введите имя",
                },
                "email_user": {
                    email: "Введите E-mail",
                    required: "Неверный формат почты",
                },
                "name_company": {
                    required: "Введите название компании или ИНН",
                },
                "checkbox": {
                    required: "",
                },
                "tel_user": {
                    required: "Введите телефон",
                    minlength: "Введите не менее 10 цифр",
                }
            },

        });
    });
    


    // Маска для формы телефона https://github.com/RobinHerbots/Inputmask

    $(".inp-tel").inputmask({
      mask: '+7 (999) 999 99-99',
      showMaskOnHover: false,
      autoUnmask: true,
    });



});



if ($("#ticker").length > 0){

var CharTimeout = 150; // скорость печатания
var StoryTimeout = 2000; // время ожидания перед переключением
var Summaries = new Array();


Summaries[0] = 'деньги';
Summaries[1] = 'время';
Summaries[2] = 'нервы';


function startTicker(){
    massiveItemCount =  Number(Summaries.length); 
    CurrentStory     = -1;
    CurrentLength    = 0;
    AnchorObject     = document.getElementById("ticker");

    


    runTheTicker();     
}

function runTheTicker(){
    var myTimeout;  
    if(CurrentLength == 0){
        CurrentStory++;
        CurrentStory      = CurrentStory % massiveItemCount;
        StorySummary      = Summaries[CurrentStory].replace(/"/g,'-');      
    }

    AnchorObject.innerHTML = StorySummary.substring(0,CurrentLength) + znak();
    if(CurrentLength != StorySummary.length){
        CurrentLength++;
        myTimeout = CharTimeout;
    } else {
        CurrentLength = 0;
        myTimeout = StoryTimeout;
    }

    if (CurrentStory == 1) {
        $('#txt-js').text('ваше');
    }
    else{
        $('#txt-js').text('ваши');
    }

    setTimeout("runTheTicker()", myTimeout);
}

function znak(){
    if(CurrentLength == StorySummary.length) return "";
    else return "|";
}

startTicker();
}
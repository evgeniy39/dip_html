 function widthAddClas(){
      $('.td-name-block-wrap').each(function(index, el) {
          var content = $(this).find('.text-td-name-block').text();
          var wsp = $(this).find('.text-td-name-block').innerWidth();
          var wblock = $(this).parent('.td-flex').width();

          if (wsp >= wblock) {
              $(this).addClass('hover');
              $(this).find('.hidden-block-name').text(content);
          }
          else{
              $(this).removeClass('hover');
              $(this).find('.hidden-block-name').text('');
          }

          
      });
      
  }




$(function () {

      // табы

    $('ul.list-tabs-content-table a').click(function(e) {
        e.preventDefault();
        var tab = $(this).attr('href');

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(tab).removeClass('active');
        }
        else{
          $('ul.list-tabs-content-table .active').removeClass('active');
          $(this).addClass('active');
          $('.item-tabs-content-table').not(tab).removeClass('active');
          $(tab).addClass('active');
        }
    });


    function navAddClass(){
      if (window.matchMedia('(min-width: 901px)').matches){
          var the_top = $(document).scrollTop();
          var tabletop = $('.table-header-js').offset().top;
          var tableMaintop = $('.table-flex').offset().top;
          var tableleft = $('.table-flex').offset().left;
          var tablewidth = $('.table-flex').innerWidth();
          var header = $('.navbar').innerHeight();
          var tp = tabletop - header;

        if (tp <= the_top) {
            $('.table-header-js').addClass('fixed');
            $('.table-header-js').css('left', tableleft + 'px');
            $('.table-header-js').css('width', tablewidth + 'px');
            if(tableMaintop > tabletop){
              $('.table-header-js').removeClass('fixed');
              $('.table-header-js').css('left', 'auto');
              $('.table-header-js').css('width', 'auto');
            }
            
        }
        
        


      }
      
    }

if ($(".table-header-js").length > 0){
  $(window).scroll(function() { 
      navAddClass();
  });

  $(window).resize(function() { 
      navAddClass();
  });

   navAddClass();
}

  



  
  setTimeout(widthAddClas, 200);


    $(window).resize(function(event) {
       widthAddClas();
    });


   // Закрытие мобильного меню при 992px

    $(window).resize(function () {
        if ($(window).width() >= 992) {
            $('.menu').removeClass('menu-opened');
            $('.toogler-enabled, .toogler-hide').removeAttr('style');
        };
    });


  $('.nav-link-collapse').on('click', function () {
    $('.nav-link-collapse').not(this).removeClass('nav-link-show');
    $(this).toggleClass('nav-link-show');
  });

  // Изменение гамбургера на крестик

  $('.navbar-toggler').on('click', function () {
    $(".menu").toggleClass('menu-opened');
    $('.toogler-hide').toggle();
    $('.toogler-enabled').toggle();
    $(['main']).toggleClass('gray-menu');
  });

  //Открытие и закрытие мобильного меню выбора компании

  $('body').on('click', '.btn-select-company', function () {
    $('.mob-select-company').addClass('mob-select-company-opened');
  });

  $('body').on('click', '.close-select', function () {
    $('.mob-select-company').removeClass('mob-select-company-opened');
  });


    // селект меню

    $(".selectmenu-page-js").selectmenu();


    // Функция количества плюс минус

    $('.minus-js').click(function () {
        var $input = $(this).parents('.pl-min-wrp').find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 0 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus-js').click(function () {
        var $input = $(this).parents('.pl-min-wrp').find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });



    // Скролл блока

    if ($(".main-price-block-calc").length > 0) {
      

      
        var h = $('.navbar').innerHeight();
        var offsetSection = $('.main-price-block-calc').offset().top - h - 40;
        var heightSection = $('.main-price-block-calc').offset().top + $('.main-price-block-calc').height() - $('.main-price-block-calc').height();

        $(window).on('scroll', function() {
          if (window.matchMedia('(min-width: 993px)').matches){
            if ($(window).scrollTop() > offsetSection) {
                    
                $('.main-price-block-calc').addClass('fixed');
            } else {
                $('.main-price-block-calc').removeClass('fixed');
            }
          }
        })
    }

     function addNoneActiveFunc(){
         $('.inp-val').each(function(index, el) {
             
            var min=$(el).prev().attr('data-min');
            if(!min){min=0;}
            if ($(el).val() == min) {
                $(el).prev().addClass('none_active');
            }
            else{
                $(el).prev().removeClass('none_active');

            }
        });
    }

    addNoneActiveFunc();
   
    $('.inp-val').change(function(event) {
         addNoneActiveFunc();
    });




     // Ползунки
     $('#slider-1').each(function(index, el) {
        $(el).slider({
            value: 10,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });

     $('#slider-2').each(function(index, el) {
        $(el).slider({
            value: 20,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });

     $('#slider-3').each(function(index, el) {
        $(el).slider({
            value: 30,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });


      $('#slider-4').each(function(index, el) {
        $(el).slider({
            value: 40,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });

      $('#slider-5').each(function(index, el) {
        $(el).slider({
            value: 50,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });

      $('#slider-6').each(function(index, el) {
        $(el).slider({
            value: 60,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });





     $('.options-link-calc-js').on('click',  function(event) {
         event.preventDefault();
         if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).parent().next('.hidden-content-block').slideUp('slow');
         }
         else {
            $(this).addClass('active');
            $(this).parent().next('.hidden-content-block').slideDown('slow');
         }
         
     });


     // табы

     $('.radio-tab-main').on('change',  function(event) {
         if ($(this).prop('checked')) {
            var id = $(this).attr('data-tab');
             $('.item-tab-calc').removeClass('active');
            $(id).addClass('active');

         }
     });


     $('.radio-tab-form').on('change',  function(event) {
         if ($(this).prop('checked')) {
            var id = $(this).attr('data-tb');
            $(this).parents('.item-tab-calc').find('.block--content-tb-calc').removeClass('active');
            $(this).parents('.item-tab-calc').find('[data-id='+ id +']').addClass('active');

         }
     });



     

     
  $(".navbar-nav > li").each(function(indx, element){
      if($(this).find("ul").is("ul")){
      $(this).addClass('toggle-link');
    }
  });

  $(".navbar-nav li.toggle-link > a").on('click', function(e) {
      e.preventDefault();

      if($(this).next("ul").is(":visible")){
            $(this).next("ul").slideUp(200);
            $(this).parent().removeClass("selected");
        } else {
            $(".navbar-nav li.toggle-link > ul").slideUp(200);
            $(this).next("ul").slideDown(200);
            $(this).parent().siblings(".navbar-nav li.toggle-link").removeClass("selected");
            $(this).parent().addClass("selected");
        }

    });
    
    









	



});





am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_material);
  // am4core.useTheme(am4themes_animated);
  // Themes end

  // Create chart instance
  var chart = am4core.create("chart_circle", am4charts.PieChart);

  // Add data
  chart.data = [ {
    "document": "Новые",
    "number": 200
  }, {
    "document": "Закрытые",
    "number": 200
  }, {
    "document": "Просроченные",
    "number": 200
  }];


  // Set inner radius
  chart.innerRadius = am4core.percent(40);

  // Add and configure Series
  var pieSeries = chart.series.push(new am4charts.PieSeries());
  var gradient = new am4core.LinearGradient();

  pieSeries.dataFields.value = "number";
  pieSeries.dataFields.category = "document";
  pieSeries.slices.template.strokeWidth = 1;
  pieSeries.slices.template.strokeOpacity = 1;
  pieSeries.labels.template.disabled = true;

  pieSeries.creditsPosition = 'top-left';


  pieSeries.colors.list = [
    am4core.color("#BDBDBD"),
    am4core.color("#71CB42"),
    am4core.color("#FC7304"),
    
  ];


  // This creates initial animation
  pieSeries.hiddenState.properties.opacity = 1;
  pieSeries.hiddenState.properties.endAngle = -90;
  pieSeries.hiddenState.properties.startAngle = -90;
});



am4core.ready(function() {
  var chart = am4core.create("chart_column", am4charts.XYChart);

  chart.data = [{
    "date": new Date(2020,5,10),
    "value": 190
  },{
    "date": new Date(2020,5,11), 
    "value": 100
  },
  {
    "date": new Date(2020,5,12), 
    "value": 140
  },{
    "date": new Date(2020,5,13), 
    "value": 120
  }, 
  {
    "date": new Date(2020,5,14), 
    "value": 150
  },{
    "date": new Date(2020,5,15), 
    "value": 65
  }];


  chart.colors.list = [
    am4core.color("#71CB42"),
  ];


  var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
  dateAxis.renderer.grid.template.location = 0.5;
  dateAxis.renderer.labels.template.location = 0.5;
  dateAxis.dateFormats.setKey("day", "dd.MM.yyyy");
  dateAxis.renderer.labels.template.fill = am4core.color("#BDBDBD");
  dateAxis.renderer.grid.template.strokeOpacity = 0;



  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.labels.template.fill = am4core.color("#000");
  valueAxis.renderer.labels.template.disabled = true;



  // Create series
  var series = chart.series.push(new am4charts.ColumnSeries());
  series.dataFields.valueY = "value";
  series.dataFields.dateX = "date";
  series.name = "value";
  series.columns.template.tooltipText = "[bold]{valueY}[/]";
  series.columns.template.fillOpacity = .8;

});



am4core.ready(function() {
  // Themes begin
  // am4core.useTheme(am4themes_animated);
  // Themes end

  // Create chart instance
  var chart = am4core.create("chart_line", am4charts.XYChart);

  // Add data
  chart.data = [
    {
      date: new Date(2020,5,12), 
      value:50
    },
    {
      date:new Date(2020,5,13), 
      value:59
    }
  ]

  chart.colors.list = [
    am4core.color("#71CB42"),
  ];


  // Create axes
  var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
  dateAxis.renderer.minGridDistance = 50;
  dateAxis.dateFormats.setKey("day", "dd.MM.yyyy");
  dateAxis.renderer.labels.template.fill = am4core.color("#BDBDBD");
  dateAxis.renderer.grid.template.strokeOpacity = 0;


  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.labels.template.fill = am4core.color("#000");

  valueAxis.renderer.inside = true;
  valueAxis.renderer.minLabelPosition = 0.05;
  valueAxis.renderer.labels.template.dy = 12;
  valueAxis.renderer.labels.template.dx = -10;


  // Create series
  var series = chart.series.push(new am4charts.LineSeries());
  series.dataFields.valueY = "value";
  series.dataFields.dateX = "date";
  series.fill = am4core.color("#71CB42");
  series.stroke = am4core.color("#71CB42");
  series.strokeWidth = 2;
  series.minBulletDistance = 10;
  series.tooltipText = "[bold]{valueY}";
  series.tooltip.pointerOrientation = "vertical";


  chart.cursor = new am4charts.XYCursor();
  // chart.cursor.xAxis = dateAxis;



});
 function widthAddClassFunc(){
      $('.td-name-block-wrap').each(function(index, el) {
          var contentbl = $(this).find('.text-td-name-block').text();
          var wspbl = $(this).find('.text-td-name-block').innerWidth();
          var wblockbl = $(this).width();


          if (wspbl >= wblockbl) {
              $(this).addClass('hover');
              $(this).find('.hidden-block-name').text(contentbl);
          }
          else{
              $(this).removeClass('hover');
              $(this).find('.hidden-block-name').text('');
          }

          
      });
      
  }

$(function () {



    if ($(".slider_banner").length > 0) {
        var swiper = new Swiper('.slider_banner', {
            slidesPerView: 1,
            spaceBetween: 20,
            loop: true,
            slidesPerGroup: 1,
            watchOverflow: true, 
            autoplay: {
                   delay: 15000,
                   disableOnInteraction: false,
                 },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    }



    function readURL(input) {
         if (input.files && input.files[0]) {
             var reader = new FileReader();
             reader.onload = function(e) {
                 $(input).parents('.req__it_avatar_wrap').find('img').attr('src', e.target.result);
             }
             reader.readAsDataURL(input.files[0]);
         }
    }

    $("#imageUpload").change(function() {
        readURL(this);
    });

    $("#imageUpload_mob").change(function() {
        readURL(this);
    });









    $(".text-td-name-bl").on('click', function(e) {
        e.preventDefault();
        if($(this).parents('.tr-flex').find(".hidden-table-flex").is(":visible")){
            $(this).parents('.tr-flex').find(".hidden-table-flex").slideUp(200);
            $(this).removeClass("active");
            widthAddClassFunc();

        } else {
            $(this).parents('.tr-flex').find(".hidden-table-flex").slideDown(200);
            $(this).addClass("active");
            widthAddClassFunc();

        }
    });

    function widthTextFunc(){
      $('.td-txt-block-wrap').each(function(index, el) {
          var content = $(this).find('.text-td-txt-block').text();
          var wsp = $(this).find('.text-td-txt-block').innerWidth();
          var wblock = $(this).parent('.td-flex').width();

          if (wsp >= wblock) {
              $(this).addClass('hover');
              $(this).find('.hidden-block-txt').text(content);
          }
          else{
              $(this).removeClass('hover');
              $(this).find('.hidden-block-txt').text('');
          }

          
      });
      
    }

    if ($(".table-flex-js").length > 0){
        setTimeout(widthTextFunc, 200);


        $(window).resize(function(event) {
           widthTextFunc();
        });

    }



    function navAddClass(){
          var the_top = $(document).scrollTop();
          var tabletop = $('.table-header-js').offset().top;
          var tableMaintop = $('.table-item-tabs-wrapper').offset().top;
          var tableleft = $('.table-item-tabs-wrap').offset().left;
          var tablewidth = $('.table-item-tabs-wrap').innerWidth();
          var tableHeight = $('.table-header-block').innerHeight();
          var header = $('.navbar').innerHeight();
          var tp = tableMaintop - header;

        if (tp < the_top) {
            $('.table-header-js').addClass('fixed');
            $('.table-header-js').css('left', tableleft + 'px');
            $('.table-header-js').css('width', tablewidth + 'px');
            $('.table-item-tabs').css('margin-top', tableHeight + 'px');

        }
        else {
            $('.table-header-js').removeClass('fixed');
            $('.table-header-js').css('left', 'auto');
            $('.table-header-js').css('width', 'auto');
            $('.table-item-tabs').css('margin-top', 'auto');
        }

      
    }

    if ($(".table-header-js").length > 0){
      $(window).scroll(function() { 
          navAddClass();
      });

      $(window).resize(function() { 
          navAddClass();
      });

       navAddClass();
    }



    // табы

    $('ul.list-tabs-content-table a').click(function(e) {
        e.preventDefault();
        $('ul.list-tabs-content-table .active').removeClass('active');
        $(this).addClass('active');
        var tab = $(this).attr('href');
        $('.item-tabs-content-table').not(tab).css({'display':'none'});
        $(tab).fadeIn(400);
    });


    $('.btn-name-pay-js').on('click',  function(event) {
       var text = $(this).text();
       $('.text-popup-pay-content').text(text);
    });

    function widthAddClas(){
        $('.name-pay-block-wrap').each(function(index, el) {

            var wLink = $(this).find('.name-pay-block').width();
            var wblock = $(this).width();

            if (wLink >= wblock) {
                $(this).find('.name-pay-block').attr('href', '#popup_text');
            }
            else{
                $(this).find('.name-pay-block').removeAttr('href');
            }

            
        });
        
    }

    widthAddClas();

    $(window).resize(function(event) {
       widthAddClas();
    });



    $(".tr-flex-parent-title").on('click', function(e) {
      if (window.matchMedia('(max-width: 768px)').matches){
        e.preventDefault();
          if($(this).next("div").is(":visible")){
              $(this).next("div").slideUp(200);
              $(this).removeClass("active");

          } else {
              $(this).next("div").slideDown(200);
              $(this).addClass("active");


          }
      }
        
    });

    function removeStyleFunc(){
      if (window.matchMedia('(min-width: 769px)').matches){
        $('.tr-flex-parent-content').removeAttr('style');
      }
    }

    $(window).resize(function(event) {
       removeStyleFunc();
    });
    removeStyleFunc();
    


    $('.menu-item-ls-js').on('click', function(event) {
      var txt = $(this).text();
      $(this).parent('.group-menu-list-select').siblings().find('.menu-item-ls-js').removeClass('active');
      $(this).addClass('active');
      $(this).parents('.select-block-page-form').find('.inp-page-select-js').val(txt);
      $(this).parents('.list-select-block-page').slideUp(200);
    });

    $('.inp-page-select-js').on('focus', function(event) {
      $(this).parents().find('.list-select-block-page').slideUp(200);
      $(this).parents().find('.title-select-block-page').removeClass('focus');


      $(this).parents('.title-select-block-page').addClass('focus');
      $(this).parent().next('.list-select-block-page').slideDown(200);
      

    });

    $('.inp-page-select-js').on('blur', function(event) {
      $(this).parents('.title-select-block-page').removeClass('focus');
    });


    $(document).mouseup(function (e){ // событие клика по веб-документу
      var div = $(".select-block-page-form"); // тут указываем ID элемента
      if (!div.is(e.target) && div.has(e.target).length === 0) { // и не по его дочерним элементам
        div.find('.list-select-block-page').slideUp(200);

        div.find('.title-select-block-page').removeClass('focus');
        div.find('.inp-page-select-js')
      }
    });


    $('body').on('change', '.inputfile-js', function(e) {
      var files = $(this).prop("files");
      var names = $.map(files, function(val) { return val.name; });
      $(this).next().text(names);
    });  


    $('.link-btn-children-js').on('click',  function(event) {
      event.preventDefault();
      var items = $(".item-child-wrap").length + 1;
      var file1 = $(this).parents('.content-item-page-profile').find('.items-children-wrapper').find(".item-child-wrap").first().find('.col-file:nth-child(1)').find('.inputfile-js').attr('name');
      var file2 = $(this).parents('.content-item-page-profile').find('.items-children-wrapper').find(".item-child-wrap").first().find('.col-file:nth-child(2)').find('.inputfile-js').attr('name');


      $(this).parents('.content-item-page-profile').find('.items-children-wrapper').find(".item-child-wrap").last().clone().find('.title-item-child')
      .text('Ребёнок ' + items).parents(".item-child-wrap").find('.inputfile-js').val('').parents(".item-child-wrap").find('.col-file:nth-child(1)').find('.inputfile-js').attr('name', file1 + '_' + items )
      .parents(".item-child-wrap").find('.col-file:nth-child(2)').find('.inputfile-js').attr('name', file2 + '_' + items ).parents(".item-child-wrap").find('.inputfile-js')
      .next().text('Выберите файл').parents(".item-child-wrap").appendTo(".items-children-wrapper");

      Filefunc();

      
    });



    $(".inp-page-select-none-text-js").keypress(function(e) {
      if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
        return false;
      }
      else if((e.which < 97 || e.which > 122) && (e.which < 65 || e.which > 90)) return false;
    });


    function funcPassword(){
      var pass = $(this).val().length;
        if (pass > 0) {
          $('.password-control').addClass('val');
        }
        else{
          $('.password-control').removeClass('val');
        }
    }

    $('#password-input').on('change',  funcPassword);
    $('#password-input').on('keyup',  funcPassword);

    

    $('body').on('click', '.password-control', function(){
      if ($('#password-input').attr('type') == 'password'){
        $(this).addClass('view');
        $('#password-input').attr('type', 'text');
      } else {
        $(this).removeClass('view');
        $('#password-input').attr('type', 'password');
      }
      return false;
    });








    // Скролл блока

    if ($(".main-price-block-calc").length > 0) {
      

      
        var h = $('.navbar').innerHeight();
        var offsetSection = $('.main-price-block-calc').offset().top - h - 40;
        var heightSection = $('.main-price-block-calc').offset().top + $('.main-price-block-calc').height() - $('.main-price-block-calc').height();

        $(window).on('scroll', function() {
          if (window.matchMedia('(min-width: 993px)').matches){
            if ($(window).scrollTop() > offsetSection) {
                    
                $('.main-price-block-calc').addClass('fixed');
            } else {
                $('.main-price-block-calc').removeClass('fixed');
            }
          }
        })
    }


    $('.title-toggle-parameters-profile-js span').on('click', function(event) {
        event.preventDefault();
        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
            $(this).parent().next().slideUp(200);
            $(this).text('Показать доп.параметры');
        }
        else{
            $(this).parent().addClass('active');
            $(this).parent().next().slideDown(200);
            $(this).text('Скрыть доп.параметры');
        }
        
    });

    // аккордеон

    $(".title-item-page-profile-js").on('click', function(e) {
        e.preventDefault();
        if (window.matchMedia('(max-width: 576px)').matches){
            if($(this).next("div").is(":visible")){
                $(this).next("div").slideUp(200);
                $(this).removeClass("active");

            } else {
                $(this).next("div").slideDown(200);
                $(this).parents().siblings().children(".title-item-page-profile-js").removeClass("active");
                $(this).addClass("active");


            }
        }
        
    });



      // Ползунки
     $('#slider-1').each(function(index, el) {
        $(el).slider({
            value: 10,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });

     $('#slider-2').each(function(index, el) {
        $(el).slider({
            value: 20,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });

     $('#slider-3').each(function(index, el) {
        $(el).slider({
            value: 30,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });


      $('#slider-4').each(function(index, el) {
        $(el).slider({
            value: 40,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });

      $('#slider-5').each(function(index, el) {
        $(el).slider({
            value: 50,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });

      $('#slider-6').each(function(index, el) {
        $(el).slider({
            value: 60,
            max: 100,
            step: 10,
            range: "min",
            create: setInputsFromSlider,
            slide: setInputsFromSlider,
            stop: setInputsFromSlider
        });

        function setInputsFromSlider(){
            $(el).parent().find('.slide-val-input').val($(el).slider("value"));
            $(el).parent().find('.slide-val-span').text($(el).slider("value") + ' руб');
        }
        setInputsFromSlider();
        
         
     });





     $('.options-link-calc-js').on('click',  function(event) {
         event.preventDefault();
         if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).parent().next('.hidden-content-block').slideUp('slow');
         }
         else {
            $(this).addClass('active');
            $(this).parent().next('.hidden-content-block').slideDown('slow');
         }
         
     });


     // табы

     $('.radio-tab-main').on('change',  function(event) {
         if ($(this).prop('checked')) {
            var id = $(this).attr('data-tab');
             $('.item-tab-calc').removeClass('active');
            $(id).addClass('active');

         }
     });


     $('.radio-tab-form').on('change',  function(event) {
         if ($(this).prop('checked')) {
            var id = $(this).attr('data-tb');
            $(this).parents('.item-tab-calc').find('.block--content-tb-calc').removeClass('active');
            $(this).parents('.item-tab-calc').find('[data-id='+ id +']').addClass('active');

         }
     });



     if ($('.calendar-pg-js').length > 0) {
        $('.calendar-pg-js').each(function(index, el) {
            $(el).datepicker({
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                monthNames : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                dayNamesMin : ['вс','пн','вт','ср','чт','пт','сб'],
            });
        });  

    }



   if ($('.calendar-page-js').length > 0) {
        $('.calendar-page-js').each(function(index, el) {
            $(el).datepicker({
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                monthNames : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                dayNamesMin : ['вс','пн','вт','ср','чт','пт','сб'],
            });
            $(el).datepicker('setDate', '+0');

             $('.btn-calendar-js').on('click',  function(event) {
                $(this).prev(el).datepicker("show");
            }); 
        });  

    }


    if ($('.calendar-date_from-js').length > 0) {
        $('.calendar-date_from-js').each(function(index, el) {
            $(el).datepicker({
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                monthNames : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                dayNamesMin : ['вс','пн','вт','ср','чт','пт','сб'],
            });
            // $(el).datepicker('setDate', '+0');

        });  

    }

    if ($('.calendar-date_to-js').length > 0) {
        $('.calendar-date_to-js').each(function(index, el) {
            $(el).datepicker({
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                monthNames : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                dayNamesMin : ['вс','пн','вт','ср','чт','пт','сб'],
            });
            // $(el).datepicker('setDate', '+0');
        });  

    }

    //Попап менеджер FancyBox
    // data-fancybox="gallery" создание галереи
    // data-caption="<b>Подпись</b><br>"  Подпись картинки
    // data-width="2048" реальная ширина изображения
    // data-height="1365" реальная высота изображения
    // data-type="ajax" загрузка контента через ajax без перезагрузки
    // data-type="iframe" загрузка iframe (содержимое с другого сайта)
    $(".fancybox").fancybox({
        hideOnContentClick: true,
        protect: false, //защита изображения от загрузки, щелкнув правой кнопкой мыши. 
        loop: true, // Бесконечная навигация по галерее
        arrows : true, // Отображение навигационные стрелки
        infobar : true, // Отображение инфобара (счетчик и стрелки вверху)
        toolbar : true, // Отображение панели инструментов (кнопки вверху)
        buttons : [ // Отображение панели инструментов по отдельности (кнопки вверху)
        // 'slideShow',
        // 'fullScreen',
        // 'thumbs',
        // 'share',
        //'download',
        //'zoom',
        'close'
        ],
        touch: false,
        animationEffect : "fade", // анимация открытия слайдов "zoom" "fade" "zoom-in-out"
        transitionEffect: 'fade', // анимация переключения слайдов "fade" "slide" "circular" "tube" "zoom-in-out" "rotate'
        animationDuration : 500, // Длительность в мс для анимации открытия / закрытия
        transitionDuration : 1366, // Длительность переключения слайдов
        slideClass : '', // Добавить свой класс слайдам

    });


    setTimeout(widthAddClassFunc, 200);


    $(window).resize(function(event) {
       widthAddClassFunc();
    });


    // селект меню 

    $(".selectmenu-page-js").selectmenu();


   $(".selectmenu-required").selectmenu({change: function( event, data ) {
        var selValue = $(this).val();
        $("#form-dowl-page").validate().element(this);
        if (selValue.length > 0) {
            $(this).next('div').removeClass("input-validation-error");
        } else {
            $(this).next('div').addClass("input-validation-error");
        }
    }});

   $("#form-dowl-page").validate({
        errorElement: "span",
        ignore: "",
        invalidHandler: function (form, validator) {
            $.each(validator.errorList, function (index, value) {
                if (value.element.nodeName.toLowerCase() == 'select') {
                    $(value.element).next('div').addClass("input-validation-error");
                }
            });
        },
        errorClass: "field-validation-error",
            highlight: function (element, errorClass) {
                $(element).addClass("input-validation-error");
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass("input-validation-error");
            },
            errorPlacement: function (error, element) {
                
            },
            rules:{
                filter_select: {
                    required:true
                }
            },
    });




    $('.toggle-link-ls-pr-lnk-js').on('click',  function(event) {
        event.preventDefault();
        $(this).toggleClass('active');
        $(this).parents('.item-list-content-info').find('.hidden-content-text').slideToggle(400);
    });

    $('.close-content-block-ls-js').on('click', function(event) {
        event.preventDefault();
       $(this).parents('.content-block-ls').slideUp(400);
       $('.link-item-inc-expen-js').text('Подробнее');
       $('body').removeClass('none_scroll');
    });


    $('.link-item-inc-expen-js').on('click', function(event) {
        event.preventDefault();
        var id = $(this).attr('href');

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(id).slideUp(400);
            $(this).text('Подробнее');
            $('body').removeClass('none_scroll');

        }
        else{
            $('.content-block-ls').slideUp(400);
            $('.link-item-inc-expen-js').text('Подробнее');
            $('.link-item-inc-expen-js').removeClass('active');
            $(this).addClass('active');
            $(id).slideDown(400);
            $(this).text('Скрыть');
            $('body').addClass('none_scroll');
        }

        
    });
    

    // аккордеон

    $(".title-toggle--content-js").on('click', function(e) {
        e.preventDefault();
        if($(this).parent().next("div").is(":visible")){
            $(this).parent().next("div").slideUp(200);
            $(this).removeClass("active");

        } else {
            
            $(this).parent().next("div").slideDown(200);
            $(this).parents().siblings().children(".title-toggle--content-js").removeClass("active");
            $(this).addClass("active");


        }
    });


    // Выезжающий блок при нажатии на цену налога/дохода или вычета/расхода

    $('.cost-green').on('click', function () {
        $('.summ-taxes').slideToggle();
    });

    $('.cost-orange').on('click', function () {
        $('.summ-deduction').slideToggle();
    });

    $('.cost-consumption-orange').on('click', function () {
        $('.history-operations-consumption').slideToggle();
    });



    // Закрытие мобильного меню при 992px

    $(window).resize(function () {
        if ($(window).width() >= 992) {
            $('.menu').removeClass('menu-opened');
            $('.toogler-enabled, .toogler-hide').removeAttr('style');
        };
    });


	$('.nav-link-collapse').on('click', function () {
		$('.nav-link-collapse').not(this).removeClass('nav-link-show');
		$(this).toggleClass('nav-link-show');
	});

	// Изменение гамбургера на крестик

	$('.navbar-toggler, .it_block_menu_toggle-js').on('click', function () {
		$(".menu").toggleClass('menu-opened');
    $("body").toggleClass('none__scroll');
    $(this).toggleClass('active');
    $(".overlay_page_menu").toggleClass('menu-opened');
		$('.toogler-hide').toggle();
		$('.toogler-enabled').toggle();
		$(['main']).toggleClass('gray-menu');
	});


  $('.close__button_js').on('click', function () {
    $(".menu").removeClass('menu-opened');
    $("body").removeClass('none__scroll');
    $('.it_block_menu_toggle-js').removeClass('active');
    $(".overlay_page_menu").removeClass('menu-opened');
    $('.toogler-hide').toggle();
    $('.toogler-enabled').toggle();
    $(['main']).toggleClass('gray-menu');
  });


	//Открытие и закрытие мобильного меню выбора компании

	$('body').on('click', '.btn-select-company', function () {
		$('.mob-select-company').addClass('mob-select-company-opened');
	});

	$('body').on('click', '.close-select', function () {
		$('.mob-select-company').removeClass('mob-select-company-opened');
	});

    $('body').on('click', '.group-list-popup_lk_page-content',  function(event) {
        event.preventDefault();
        $('.group-list-popup_lk_page-content').removeClass('active');
        $(this).addClass('active');
    });




    $(".star-border").click(function(){
        var valStar = $(this).attr('data-val');
        $(this).parents('.send-block').find('#rating').val(valStar);
      $(this).find(".star").addClass('active');
      if ($(this).find(".star").hasClass('error')) {
         $(".star").removeClass('error');
      }
      $(this).parent('li').prevAll().find(".star").addClass('active');
      $(this).parent('li').nextAll().find(".star").removeClass('active');

    });

    

    $('input[name="rating_checkbox"]').on('change', function(event) {
        if ($(this).prop('checked')) {
            $(this).parents('.right-block-form').find('.rating-form-block').addClass('active');
        }
        else{
            $(this).parents('.right-block-form').find('.rating-form-block').removeClass('active');
        }
    });

    $('.link-back-block-js').on('click',  function(event) {
        event.preventDefault();
       $('.main-block-content-dialogues').removeClass('active');
       $('.right-block-content-dialogues').addClass('active');
    });



        // аккордеон

    $(".title-group-ls-accor-dialog").on('click', function(e) {
        e.preventDefault();
        if($(this).next("div").is(":visible")){
            $(this).next("div").slideUp(200);
            $(this).removeClass("active");

        } else {
            // $(".content-group-ls-accor-dialog").slideUp(200);
            $(this).next("div").slideDown(200);
            // $(this).parents().siblings().children(".title-group-ls-accor-dialog").removeClass("active");
            $(this).addClass("active");


        }
    });


    $('.link-group-list-dialog-page').on('click', function(event) {
        event.preventDefault();
        if (window.matchMedia('(max-width: 850px)').matches){
            $('.right-block-content-dialogues').removeClass('active');
            $('.main-block-content-dialogues').addClass('active');
                 $(".scrollbar-dialogues").mCustomScrollbar("scrollTo","bottom",{
                    scrollInertia:0
             });
        }
        
    });



        // Скролл // Документация http://manos.malihu.gr/jquery-custom-content-scroller/

    $(".scrollbar").mCustomScrollbar({
        autoHideScrollbar: false,
        scrollInertia: 400,
        scrollButtons:{ 
            enable: true 
        },
    });



    if ($(".scrollbar-dialogues").length > 0) {
      let scroll_chat = $(".scrollbar-dialogues");

      OverlayScrollbars(scroll_chat, { 
        scrollbars : {
          clickScrolling : true
        },
        callbacks : {
          onInitialized: function(){
            $('.load_dialogues').addClass('selected');
          },
        },
      }).scroll({ y : "100%"  });
    }

    // if ($('#scrollbar__dialogues').length > 0) {
        
    //     let scrollList;
    //         scrollList = OverlayScrollbars(document.getElementById('scrollbar__dialogues'), {
    //             scrollbars : {
    //               clickScrolling : true
    //             },
    //             callbacks : {
    //                 onInitialized: function(e) {
    //                     setTimeout(function(){
    //                         scrollList.scroll({ y : "100%" });
    //                         $('.load_dialogues').addClass('selected');
    //                     }, 500);
                        
    //                 },    
    //             }
    //         });

    // }







	// Функция количества плюс минус

    $('.minus-js').click(function () {
        var $input = $(this).parents('.pl-min-wrp').find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 0 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus-js').click(function () {
        var $input = $(this).parents('.pl-min-wrp').find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });


    function addNoneActiveFunc(){
         $('.inp-val').each(function(index, el) {
             
            var min=$(el).prev().attr('data-min');
            if(!min){min=0;}
            if ($(el).val() == min) {
                $(el).prev().addClass('none_active');
            }
            else{
                $(el).prev().removeClass('none_active');

            }
        });
    }

    addNoneActiveFunc();
   
    $('.inp-val').change(function(event) {
         addNoneActiveFunc();
    });


	// табы

  /*$('ul.tabs-accou-main a').click(function(e) {
        e.preventDefault();
        $('ul.tabs-accou-main .active').removeClass('active');
        $(this).addClass('active');
        var tab = $(this).attr('href');
        $('.item-tabs-content-accou-main').not(tab).css({'display':'none'});
        $(tab).fadeIn(400);
    });*/ 




	$('.item-card-js').on('click',  function(event) {
		event.preventDefault();

		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$(this).parent().removeClass('col-12 mb-3').addClass('col-xl-4 col-lg-4 col-6 mb-3');
			$(this).parent().siblings().find('.item-card-js').removeClass('none_active').parent().removeClass('none_active');

			$(this).find('.visible-part-card').removeClass('col-6 visible-part-card').addClass('col-12 visible-part-card');

			$(this).find('.hidden-part-card').addClass('d-none');

		}
		else{
			$(this).addClass('active');
			$(this).parent().removeClass('col-xl-4 col-lg-4 col-6 mb-3').addClass('col-12 mb-3');
			$(this).parent().siblings().find('.item-card-js').addClass('none_active').parent().addClass('none_active');	

			$(this).find('.visible-part-card').removeClass('col-12 visible-part-card').addClass('col-6 visible-part-card');
			$(this).find('.hidden-part-card').removeClass('d-none');
		}


		
		
	});

    $(".fanc").fancybox();



    if ($("#zdrop").length > 0) {
        initFileUploader("#zdrop");
        function initFileUploader(target) {


            var previewNode = document.querySelector(".zdrop-template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            

            var zdrop = new Dropzone(target, {
                url: 'upload.php',
                maxFilesize:10,
                paramName: 'file',
                maxFilesize:30,
                acceptedFiles: "image/jpeg,image/png,application/pdf",
                previewTemplate: previewTemplate,
                previewsContainer: ".previews-file",
                hiddenInputContainer:'.file-page',
                clickable: "#upload-label",
                chunksUploaded: function(file, done) {
                     
                }
            });

            

            zdrop.on("addedfile", function(file) { 
                $('.preview-container').css('visibility', 'visible');
                $('.num-file-js').text(zdrop.files.length);
                if (zdrop.files.length > 1) {
                    $('.name-file-page-link, .popupUpload').addClass('active');
                    $('.name-file-page-wrap').addClass('none_active');
                }
                else{
                    
                    $.fancybox.close($('#popupUpload-1'));
                    $(".fanc").fancybox({
                        afterClose : function(instance, current) {
                            $('#popupUpload-1').removeAttr('style');
                        }
                    });
                    $('.name-file-page-link, .popupUpload').removeClass('active');
                    $('.name-file-page-wrap').removeClass('none_active');
                    
                    
                }
            });

            zdrop.on("removedfile", function(file) { 
                $('.num-file-js').text(zdrop.files.length);
                if (zdrop.files.length > 1) {
                    $('.name-file-page-link, .popupUpload').addClass('active');
                    $('.name-file-page-wrap').addClass('none_active');
                }
                else{

                    $.fancybox.close($('#popupUpload-1'));
                    $(".fanc").fancybox({
                        afterClose : function(instance, current) {
                            $('#popupUpload-1').removeAttr('style');
                        }
                    });
                    $('.name-file-page-link, .popupUpload').removeClass('active');
                    $('.name-file-page-wrap').removeClass('none_active');
                    
                    
                }
               
            });
     
                    
          }
    }



    






    if ($("#zdrop_upload").length > 0) {
        initFileUploader("#zdrop_upload");
        function initFileUploader(target) {


            var previewNode = document.querySelector(".zdrop-template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            

            var zdrop = new Dropzone(target, {
                url: 'upload.php',
                maxFilesize:10,
                paramName: 'file',
                maxFilesize:30,
                acceptedFiles: "image/jpeg,image/png,application/pdf",
                previewTemplate: previewTemplate,
                previewsContainer: ".previews-file",
                hiddenInputContainer:'.file-page-block',
                clickable: "#upload-label",
                chunksUploaded: function(file, done) {
                     
                }
            });

        
                    
          }
    }





    if ($("#zdrop_pay").length > 0) {
        initFileUploader("#zdrop_pay");
        function initFileUploader(target) {


            var previewNode = document.querySelector(".zdrop-template-pay");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            

            var zdrop = new Dropzone(target, {
                url: 'upload.php',
                maxFilesize:10,
                paramName: 'file',
                maxFilesize:30,
                acceptedFiles: "image/jpeg,image/png,application/pdf",
                previewTemplate: previewTemplate,
                previewsContainer: ".previews-file-pay",
                hiddenInputContainer:'.file__pay_block',
                clickable: "#upload-label-pay",
                chunksUploaded: function(file, done) {
                     
                }
            });

        
                    
          }
    }

























	
  $('.tel-btn-tablet-js').on('click',  function(event) {
    event.preventDefault();
    $('.contacts-block-header').toggleClass('active');
  });

  $('.close-contacts-js').on('click',  function(event) {
    event.preventDefault();
    $('.contacts-block-header').removeClass('active');
  });






  function funcSelectAcc(){
    $(".selectmenu-page-acc-js").selectmenu();
  }

  var counterProduct = 0;
  var counterService = 0;
  var priceProduct = ['21 000', '22 000', '23 000', '24 000', '25 000', '26 000'];
  var priceService = ['21 000', '22 000', '23 000', '24 000', '25 000', '26 000'];


  function addAccProductFunc(event){
    event.preventDefault();  
    var html = 
    '<div class="block-account-add-lk-ls">' +
        '<div class="block-account-add-lk-ls-wrap">' +
            '<div class="col-add-lk col-add-lk_bl-1">' +
                '<div class="search-filter-oper">' +
                    '<input type="text" name="search_product_' + counterProduct + '" class="input-search input-bl" placeholder="Товар" autocomplete="off">' +
                    '<button class="button-search" type="submit"></button>' +
                    '<div class="list-search-block">' +
                        '<button class="button-close-select button-close-select-js" type="button">' +
                            '<svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">' +
                                '<line x1="1.70711" y1="1.29289" x2="13.7279" y2="13.3137" stroke="#E6E6E6" stroke-width="2"/>' +
                                '<line x1="1.29289" y1="13.2929" x2="13.3137" y2="1.27208" stroke="#E6E6E6" stroke-width="2"/>' +
                            '</svg>' +
                        '</button>' +
                        '<div class="list-search-block-wrap">' +
                            '<ul class="ls-search-block">' +
                                '<li class="group-ls-search-bl"><span>Выбор_списка_1</span></li>' +
                                '<li class="group-ls-search-bl"><span>Выбор_списка_2</span></li>' +
                                '<li class="group-ls-search-bl"><span>Выбор_списка_3</span></li>' +
                            '</ul>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-2">' +
               ' <input type="text" name="number_product_' + counterProduct + '" class="input-bl" placeholder="Кол-во">' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-3">' +
                '<select name="info_product_' + counterProduct + '" class="selectmenu-page-content selectmenu-page-acc-js" >' +
                    '<option disabled selected value="шт.">шт.</option>' +
                    '<option value="шт.">шт.</option>' +
                    '<option value="шт.">шт.</option>' +
                '</select>' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-4">' +
                '<input type="text" name="price_product_' + counterProduct + '" class="input-bl" placeholder="Цена">' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-4">' +
                '<input type="text" name="ndc_product_' + counterProduct + '" class="input-bl" placeholder="НДС">' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-5">' +
                '<div class="price-product-account"><span>'+ priceProduct[counterProduct] +'</span> ₽</div>' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-6">' +
                '<button class="delete-block-btn delete-block-btn-js">Удалить</button>' +
            '</div>' +
        '</div>' +
    '</div>';

    counterProduct++;


    $(this).parents('.item-account-add-lk').find('.blocks-account-add-lk-ls').append(html);
    funcSelectAcc();
  }

  function addAccServiceFunc(event){
    event.preventDefault();  
    var html = 
    '<div class="block-account-add-lk-ls">' +
        '<div class="block-account-add-lk-ls-wrap">' +
            '<div class="col-add-lk col-add-lk_bl-1">' +
                '<div class="search-filter-oper">' +
                    '<input type="text" name="search_service_' + counterService + '" class="input-search input-bl" placeholder="Услуга" autocomplete="off">' +
                    '<button class="button-search" type="submit"></button>' +
                    '<div class="list-search-block">' +
                        '<button class="button-close-select button-close-select-js" type="button">' +
                            '<svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">' +
                                '<line x1="1.70711" y1="1.29289" x2="13.7279" y2="13.3137" stroke="#E6E6E6" stroke-width="2"/>' +
                                '<line x1="1.29289" y1="13.2929" x2="13.3137" y2="1.27208" stroke="#E6E6E6" stroke-width="2"/>' +
                            '</svg>' +
                        '</button>' +
                        '<div class="list-search-block-wrap">' +
                            '<ul class="ls-search-block">' +
                                '<li class="group-ls-search-bl"><span>Выбор_списка_1</span></li>' +
                                '<li class="group-ls-search-bl"><span>Выбор_списка_2</span></li>' +
                                '<li class="group-ls-search-bl"><span>Выбор_списка_3</span></li>' +
                            '</ul>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-2">' +
               ' <input type="text" name="number_service_' + counterService + '" class="input-bl" placeholder="Кол-во">' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-3">' +
                '<select name="info_service_' + counterService + '" class="selectmenu-page-content selectmenu-page-acc-js" >' +
                    '<option disabled selected value="шт.">шт.</option>' +
                    '<option value="шт.">шт.</option>' +
                    '<option value="шт.">шт.</option>' +
                '</select>' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-4">' +
                '<input type="text" name="price_service_' + counterService + '" class="input-bl" placeholder="Цена">' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-4">' +
                '<input type="text" name="ndc_service_' + counterService + '" class="input-bl" placeholder="НДС">' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-5">' +
                '<div class="price-product-account"><span>'+ priceService[counterService] +'</span> ₽</div>' +
            '</div>' +
            '<div class="col-add-lk col-add-lk_bl-6">' +
                '<button class="delete-block-btn delete-block-btn-js">Удалить</button>' +
            '</div>' +
        '</div>' +
    '</div>';

    counterService++;


    $(this).parents('.item-account-add-lk').find('.blocks-account-add-lk-ls').append(html);
    funcSelectAcc();
  }

  function removeItemFunc(){
    $(this).parents('.block-account-add-lk-ls').remove();
  }


  $('body').on('click', '.delete-block-btn-js', removeItemFunc);
  $('body').on('click', '.btn-add_account-product-js', addAccProductFunc);
  $('body').on('click', '.btn-add_account-service-js', addAccServiceFunc);


 


});





//input file

var inputs = document.querySelectorAll('.inputfile');
Array.prototype.forEach.call(inputs, function(input){
  var label  = input.nextElementSibling,
      labelVal = label.innerHTML;
  input.addEventListener('change', function(e){
    var fileName = '';
    if( this.files && this.files.length > 1 )
      fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
    else
      fileName = e.target.value.split( '\\' ).pop();
    if( fileName )
      label.querySelector( 'span' ).innerHTML = fileName;
    else
      label.innerHTML = labelVal;
  });
});















